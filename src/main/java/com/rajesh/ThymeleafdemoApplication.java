package com.rajesh;

import java.util.ArrayList;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
public class ThymeleafdemoApplication {

	public static void main(String[] args) {
		ApplicationContext ctx = SpringApplication.run(ThymeleafdemoApplication.class, args);
		
		String[] names = ctx.getBeanDefinitionNames();
		for(String name:names) {
			System.out.println(name);
		}
	}
}
