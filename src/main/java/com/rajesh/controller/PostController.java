package com.rajesh.controller;

import java.util.ArrayList;
import java.util.Date;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.rajesh.domain.Post;

@Controller
@RequestMapping("/posts")
public class PostController {

	@RequestMapping("/")
	public String list(Model model) {
		model.addAttribute("pageTitle", "My Custom Page Titel");
		model.addAttribute("posts", createPosts());
		return "views/list";
		}

	private ArrayList<Post> createPosts() {
		// TODO Auto-generated method stub
		//Post1
		Post post1 = new Post();
		post1.setTitle("My Blog post 1");
		post1.setPosted(new Date());
		post1.setAuthor("Rajesh");
		post1.setBody(getPostBody());
		
		Post post2 = new Post();
		post2.setTitle("My Blog post 2");
		post2.setPosted(new Date());
		post2.setAuthor("Rajesh");
		post2.setBody("<p>The post is the shortest one of all</p>");
		
		ArrayList<Post> posts = new ArrayList<Post>();
		posts.add(post1);
		posts.add(post2);
		
		return posts;
	}

	private String getPostBody() {
		// TODO Auto-generated method stub
		String body = "<p> This is really a big post</p>";
		  body += "<p> Second line of my post</p>";
		  
		return body;
	}
}
